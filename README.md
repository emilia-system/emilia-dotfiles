# Emilia DotFiles

[![pipeline status](https://gitlab.com/emilia-system/emilia-dotfiles/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-dotfiles/commits/master)
[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)

The dotfiles that resides in /etc/skel

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia DotFiles pt_BR

Os arquivos de configuração que residem em /etc/skel

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)
