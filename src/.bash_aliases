#
# ~/.bash_aliases
#

# Alias made for the Emilia System. put yours down here.

# Popular aliases
alias ll='ls -la'
alias lt='ls --human-readable --size -l -S --classify'
alias ping='ping -c 5'
alias df='df -H'
alias du='du -ch'
alias mount='mount | column -t'
alias untar='tar -xvf'
# Shortcuts
alias c='clear'
alias cls='clear'

# "Custom" commands for usability.
alias count-files='find . -type f | wc -l'
alias now='date +"%d-%m-%Y - %T"'

# System admin
alias update-system='pacman -Syu'
alias update-aur='yay --aur -Syu'

# Commands with spaces. Needed because with that space before it, the command works with aliases.
alias sudo='sudo '
alias time='time '

# Ubuntu defaults. In testing...
    # example use: command; alert "Finished command"
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
